package com.company;

import java.util.Scanner;

public class Imprimir {
    private Scanner leitura = new Scanner(System.in);
    private CacaNiquel cacaNiquel = new CacaNiquel();

    public void init() {

        mensagemUsuario("Ola! :3 Vamos jogar Caça Níquel? $$$$$$$$$$$ ");
        mensagemUsuario("Digite '1' para jogar ou digite '0' para sair:");

        int entrada = leitura.nextInt();
        if (entrada > 0) {
            cacaNiquel.sortear();
            mensagemUsuario("O resultado da partida foi: " + cacaNiquel.valorPartida + ". Resultado total = " + cacaNiquel.resultado);
        } else {
            return;
        }
    }

    private static void mensagemUsuario(String mensagem) {
        System.out.println(mensagem);
    }
}
