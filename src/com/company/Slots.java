package com.company;

public enum Slots {
    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    SETE(300);

    public double valor;

    public double getValor() {
        return valor;
    }

    Slots(double valor) {
        this.valor = valor;
    }
}
