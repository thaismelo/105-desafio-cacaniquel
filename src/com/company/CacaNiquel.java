package com.company;
import java.util.ArrayList;
import java.util.Random;

public class CacaNiquel extends Jogo {
    private ArrayList<Slots> resultadoSlots;
    private int quantidadeSlots = 3;

    public void sortear() {
        Random random = new Random();
        resultadoSlots = new ArrayList<>();

        for (int i = 0; i < quantidadeSlots; i++) {
            int numeroSorteado = random.nextInt(Slots.values().length);
            this.resultadoSlots.add(Slots.values()[numeroSorteado]);
        }
        System.out.println(resultadoSlots);

        this.verificarSimbolosIguais();
    }

    public void verificarSimbolosIguais() {
        this.valorPartida();

        boolean todosIguais = resultadoSlots.stream().distinct().limit(resultadoSlots.size()).count() == 1;

        if (todosIguais) {
            this.resultado = this.resultado * 100;
        }
    }

    public void valorPartida(){
        this.valorPartida = 0;

        for (int i = 0; i < resultadoSlots.size(); i++) {
            somarResultado(resultadoSlots.get(i).valor);
        }

        this.resultado = this.valorPartida;
    }

    @Override
    public void somarResultado(double valorPartida) {
        this.valorPartida += valorPartida;
    }
}
