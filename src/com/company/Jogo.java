package com.company;

public abstract class Jogo {
    protected double resultado;
    protected double valorPartida;

    public double getValorPartida() {
        return valorPartida;
    }

    public void setValorPartida(double valorPartida) {
        this.valorPartida = valorPartida;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }

    public abstract void somarResultado(double valorPartida);


}
